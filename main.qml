import QtQuick 2.15
import QtQuick.Window 2.15
import "code.js" as Activity
Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Local Number System Test")
    Rectangle {
        width: parent.width
        height: 40
        border.color: "black"
        border.width: 2
        radius: 10

        Text {
            id: beng
            x :20
            y:10
            text: Activity.func('beng');
        }
    }
}
